import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestpaieComponent } from './testpaie.component';

describe('TestpaieComponent', () => {
  let component: TestpaieComponent;
  let fixture: ComponentFixture<TestpaieComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestpaieComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TestpaieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
