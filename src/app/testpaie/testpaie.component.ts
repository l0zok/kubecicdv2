import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { PaiementService } from 'src/app/services/paiement.service';
import { GlobalConstants } from 'src/app/shared/global-constants';

@Component({
  selector: 'app-testpaie',
  templateUrl: './testpaie.component.html',
  styleUrls: ['./testpaie.component.scss']
})
export class TestpaieComponent implements  OnInit{

  dataSource:any;
  responseMessage:any;
  errorMessage! : string;
  paiementFilter: any={account:''};

  currentPage : number=0;
  pageSize : number=10;
  u: number = 1;
  totalPaiement:any;

  /*nouvelle pagination*/
  currentPage1: number = 1;
  itemsPerPage1: number = 10;
  totalItems1: any;
  totalPages1: any;
  itemsToDisplay1: any[] | undefined;
  currentPageNumber1: any;
  maxVisiblePages: number = 5; // Nombre maximal de numéros de pagination à afficher
  ellipsisThreshold: number = 5; // A partir de quel numéro de page les points de suspension seront affichés
   /*nouvelle pagination*/

  constructor(private ngxService:NgxUiLoaderService,
    private paiementService:PaiementService,
    private router: Router) { }

    ngOnInit(): void {
      //this.ngxService.start();
      //this.tableData();
      this.fetchData();
    }

    tableData(){
      this.paiementService.getPaiements().subscribe((response:any)=>{
        //this.ngxService.stop();
       
        this.dataSource = response.datas;
        this.totalPaiement = response.count;
        console.log(this.totalPaiement);
      },(error:any)=>{
        //this.ngxService.stop();
        if(error.error?.message){
          this.responseMessage = error.error?.message;
        }else{
          this.responseMessage = GlobalConstants.genericError;
        }
        //this.snackbarService.openSnackBar(this.responseMessage,GlobalConstants.error);
      })
    }

    handleDetailPaiement(id: string){
      this.router.navigateByUrl("/arkham/detail-paiement/"+id);
    }

    fetchData() {
      this.paiementService.getApiUrl(this.currentPage1, this.itemsPerPage1).subscribe((response: any) => {
          this.totalItems1 = response.count;
          this.totalPages1 = Math.ceil(this.totalItems1 / this.itemsPerPage1);
          this.itemsToDisplay1 = response.datas;
          this.currentPageNumber1 = this.currentPage1;
          console.log(this.currentPageNumber1)
        });
    }
    
    getPaginationNumbers(): (number | string)[] {
      const totalPages1 = Math.ceil(this.totalItems1 / this.itemsPerPage1);
      const currentPage1 = this.currentPageNumber1;
    
      if (totalPages1 <= this.maxVisiblePages) {
        return Array.from({ length: totalPages1 }, (_, i) => i + 1);
      } else {
        let startPage: number;
        let endPage: number;
    
        if (currentPage1 <= this.ellipsisThreshold) {
          startPage = 1;
          endPage = this.maxVisiblePages;
        } else if (currentPage1 + this.ellipsisThreshold > totalPages1) {
          startPage = totalPages1 - this.maxVisiblePages + 1;
          endPage = totalPages1;
        } else {
          startPage = currentPage1 - Math.floor((this.maxVisiblePages - 1) / 2);
          endPage = currentPage1 + Math.floor(this.maxVisiblePages / 2);
        }
    
        const paginationNumbers: (number | string)[] = [];
    
        if (startPage > 1) {
          paginationNumbers.push(1);
        }
    
        if (startPage > 2) {
          paginationNumbers.push('ellipsis');
        }
    
        for (let i = startPage; i <= endPage; i++) {
          paginationNumbers.push(i);
        }
    
        if (endPage < totalPages1 - 1) {
          paginationNumbers.push('ellipsis');
        }
    
        if (endPage < totalPages1) {
          paginationNumbers.push(totalPages1);
        }
    
        return paginationNumbers;
      }
    }

    previousPage() {
      if (this.currentPage1 > 1) {
        this.currentPage1--;
        this.fetchData();
      }
    }
  
    nextPage() {
      if (this.currentPage1 < this.totalPages1) {
        this.currentPage1++;
        this.fetchData();
        console.log(this.fetchData());
      }
    }
  
    getPageNumbers(): number[] {
      const pageNumbers1 = [];
      for (let i = 1; i <= this.totalPages1; i++) {
        pageNumbers1.push(i);
      }
      return pageNumbers1;
    }
  
    /*goToPage(pageNumber1: number | string) {
      if (pageNumber1 >= 1 && pageNumber1 <= this.totalPages1) {
        this.currentPage1 = pageNumber1;
        this.fetchData();
      }
    }*/

    goToPage(pageNumber1: number | string) {
      if (typeof pageNumber1 === 'number') {
        this.currentPage1 = pageNumber1;
        this.fetchData();
      }
    }

}
