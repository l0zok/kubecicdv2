import { Injectable } from "@angular/core";

export interface Menu{
    state: string;
    name: string;
    icon: string;
    role: string;
}

const MENUPARAMITEMS = [
    {state:'channel', name:'Channel',icon:'people',role:''},
    {state:'user', name:'Compte utilisateur',icon:'people',role:''},
    {state:'marchant', name:'Compte marchant',icon:'people',role:''},
    {state:'role', name:'Role',icon:'people',role:''}
];



@Injectable()
export class MenuParamItems {
    getMenuParamitem():Menu[] {
        return MENUPARAMITEMS;
    }
}