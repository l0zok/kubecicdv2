import { Injectable } from "@angular/core";

export interface Menu{
    state: string;
    name: string;
    icon: string;
    role: string;
}

const MENUITEMS = [
    {state:'dashboard', name:'Dashboard',icon:'dashboard',role:''},
    {state:'transfert', name:'Transfert et Validation',icon:'list_alt',role:''},
    {state:'paiement', name:'Paiement',icon:'import_contacts',role:''}
    //{state:'user', name:'Compte utilisateur',icon:'people',role:'admin'},
    //{state:'marchant', name:'Compte marchant',icon:'people',role:'admin'}
];


@Injectable()
export class MenuItems {
    getMenuitem():Menu[] {
        return MENUITEMS;
    }
}