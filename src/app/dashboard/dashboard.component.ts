import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SnackbarService } from '../services/snackbar.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { DashboardService } from '../services/dashboard.service';
import { GlobalConstants } from '../shared/global-constants';
import * as Highcharts from 'highcharts';
import { SeriesColumnOptions } from 'highcharts';
import Exporting from 'highcharts/modules/exporting';
import { format, startOfMonth, endOfMonth, subDays } from 'date-fns';

import { Options } from 'highcharts';
import { DateRange } from 'ngx-daterangepicker-material/daterangepicker.component';
import * as moment from 'moment';
import { DaterangepickerDirective } from 'ngx-daterangepicker-material';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements AfterViewInit {
  @ViewChild('chartContainer', { static: true }) chartContainer!: ElementRef;
  @ViewChild('chartContainer2', { static: true }) chartContainer2!: ElementRef;
  @ViewChild('closebutton') closebutton: any;
  @ViewChild(DaterangepickerDirective, { static: false }) pickerDirective!: DaterangepickerDirective;
  @ViewChild(DaterangepickerDirective, {static: true}) picker!: DaterangepickerDirective;
  selected!: {startDate1: moment.Moment, endDate2: moment.Moment};

  startDate: Date = new Date();
  endDate: Date = new Date();
  datedebut!: string;
  datefin!: string;
  titreSelect:any=`Aujourd'hui`

  state: any = "Done";

  itemsTolistPayement!: any[];
  itemsTolistPayementStatus!: any[];

  responseMessage: any;
  data: any;
  userconnectinfo!: any;

  formValue!: FormGroup;
  dateSelect!: FormGroup;

  selectedRange!: DateRange;

  data1 = [{
    name: 'Done',
    y: 0
  }, {
    name: 'Failed',
    y: 0
  }, {
    name: 'InProgress',
    y: 0
  }];

  data11=[{
    name: 'Done',
    y: 0
  }, {
    name: 'Failed',
    y: 0
  }, {
    name: 'InProgress',
    y: 0
  }];

  data2 = [
    {
      name: '00h',
      y: 0
    },
    {
      name: '01h',
      y: 0
    },
    {
      name: '02h',
      y: 0
    },
    {
      name: '03h',
      y: 0
    },
    {
      name: '04h',
      y: 0
    },
    {
      name: '05h',
      y: 0
    },
    {
      name: '06h',
      y: 0
    },
    {
      name: '07h',
      y: 0
    },
    {
      name: '08h',
      y: 0
    },
    {
      name: '09h',
      y: 0
    }
  ];

  data22=this.data2;

  ngAfterViewInit() {
    this.createChart();
  }

  constructor(
    private DashboardService: DashboardService,
    private ngxService: NgxUiLoaderService,
    private snackbarService: SnackbarService,
    private formBuilder: FormBuilder,
  ) {
    this.formValue = this.formBuilder.group({
      startDate:  [''],
      endDate: [''],
      state: [''],
    })

    this.dateSelect = this.formBuilder.group({
      selected:  [''],
    })
    
    this.startDate = new Date();
    this.endDate = new Date();

    this.datedebut=format(this.startDate, 'dd/MM/yyyy');
    this.datefin= format(this.endDate, 'dd/MM/yyyy');

    this.dashboarData();
    this.getInfoStatuData();
    this.getInfoData();
    Exporting(Highcharts);
  }

  chart!: Highcharts.Chart;
  chart2!: Highcharts.Chart;
  Highcharts: typeof Highcharts = Highcharts;
  chartOptions: Highcharts.Options = {
    chart: {
      plotBackgroundColor: 'white',
      plotBorderWidth: 0,
      plotBorderColor: 'white',
      plotShadow: false,
      type: 'pie'
    },
    title: {
      text: `Statistique des paiements par statut`
    },
    tooltip: {
      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %'
        }
      }
    },
    series: [{
      type: 'pie',
      name: 'Brands',
      data: this.data1
    }],
    credits: {
      enabled: false
    },
    exporting: {
      enabled: true
    }
  };


  // Create the chart column
  chartOptionsColum: Highcharts.Options = {
    chart: {
      type: 'column'
    },
    title: {
      align: 'center',
      text: `Statistique des paiements par nombre `
    },
    subtitle: {
      align: 'left',
      text: ''
    },
    accessibility: {
      announceNewData: {
        enabled: true
      }
    },
    xAxis: {
      type: 'category'
    },
    yAxis: {
      title: {
        text: 'Total percent market share'
      }

    },
    legend: {
      enabled: false
    },
    plotOptions: {
      series: {
        borderWidth: 0,
        dataLabels: {
          enabled: true,
          format: '{point.y}'
        }
      }
    },

    tooltip: {
      headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
      pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
    },

    credits: {
      enabled: false
    },
    series: [
      {
        type: 'column',
        name: 'Browsers',
        colorByPoint: true,
        data: this.data2
      }
    ] as SeriesColumnOptions[]
  };

  dashboarData() {
    this.DashboardService.getDetails().subscribe({
      next: (response: any) => {
        this.ngxService.stop();
        this.data = response;
        this.userconnectinfo = localStorage.getItem('userconnectinfo');
      },
      error: (error: any) => {
        this.ngxService.stop();
        if (error.error?.message) {
          this.responseMessage = error.error?.message;
        } else {
          this.responseMessage = GlobalConstants.genericError;
        }
      }
    });
  }

  getInfoStatuData() {
    
    this.DashboardService.getInfoStatus(this.datedebut, this.datefin).subscribe({
      next: (response: any) => {
        this.ngxService.stop();
        this.data = response;
        this.itemsTolistPayement = response.datas;

        if(response.datas){
          this.itemsTolistPayement = this.itemsTolistPayement.map((item) => ({
            name: 'false',
            y: 'false'
          }));
  
          const dataArray: { name: string; y: number }[] = response.datas.map((item: any) => ({
            name: item.key,
            y: item.value
          }));
          this.userconnectinfo = localStorage.getItem('userconnectinfo');
  
          const convertedData = response.datas.map((item: any) => {
            return {
              name: item.key,
              y: item.value
            };
          });
          this.data1 = convertedData;
          if (this.chart) {
            this.chart.series[0].setData(this.data1);
            this.chart.redraw();
          }
        }else{
          this.data1 = this.data11;
          if (this.chart) {
            this.chart.series[0].setData(this.data1);
            this.chart.redraw();
          }
        }
        
      },
      error: (error: any) => {
        this.ngxService.stop();
        if (error.error?.message) {
          this.responseMessage = error.error?.message;
        } else {
          this.responseMessage = GlobalConstants.genericError;
        }
      }
    });
  }

  getInfoData() {
    this.DashboardService.getInfo(this.datedebut, this.datefin).subscribe({
      next: (response: any) => {
        this.ngxService.stop();
        this.data = response;
        this.itemsTolistPayementStatus = response.datas;
        

        if(response.datas){
          this.itemsTolistPayementStatus = this.itemsTolistPayementStatus.map((item) => ({
            name: 'false',
            y: 'false'
          }));

          const dataArrayStatus: { name: string; y: number }[] = response.datas.map((item: any) => ({
            name: item.key,
            y: item.value
          }));
  
          const convertedData2 = response.datas.map((item: any) => {
            return {
              name: item.key,
              y: item.value
            };
          });
          this.data2 = convertedData2;
  
          if (this.chart2) {
            this.chart2.series[0].setData(this.data2);
            this.chart2.redraw();
          }
        }else{
          this.data2 = this.data22;
  
          if (this.chart2) {
            this.chart2.series[0].setData(this.data2);
            this.chart2.redraw();
          }
        }

      },
      error: (error: any) => {
        this.ngxService.stop();
        if (error.error?.message) {
          this.responseMessage = error.error?.message;
        } else {
          this.responseMessage = GlobalConstants.genericError;
        }
      }
    });
  }

  createChart() {
    this.chart = Highcharts.chart(this.chartContainer.nativeElement, this.chartOptions);
    this.chart2 =Highcharts.chart(this.chartContainer2.nativeElement, this.chartOptionsColum);
  }

  Recherche() {
    const startDateValue = this.formValue.value.startDate;
    const endDateValue = this.formValue.value.endDate;
    this.titreSelect=`Personnaliser`;

    // Vérifier si les dates sont valides
    if (startDateValue && endDateValue) {
      // Convertir les valeurs en objets de date
      const startDate = new Date(startDateValue);
      const endDate = new Date(endDateValue);
      this.state = this.formValue.value.state;
  
      // Vérifier si les dates sont valides avant de les formater
      if (!isNaN(startDate.getTime()) && !isNaN(endDate.getTime())) {
        this.startDate = startDate;
        this.endDate = endDate;
  
        this.datedebut = format(this.startDate, 'dd/MM/yyyy');
        this.datefin = format(this.endDate, 'dd/MM/yyyy');
        this.getInfoStatuData();
        this.getInfoData();
        this.closebutton.nativeElement.click();
        //console.log(this.datedebut);
      } else {
        console.log('Dates invalides');
      }
    }
  }

  RechercheMois() {

    // Récupérer la date de début du mois en cours
    this.titreSelect=`Ce mois`;
    const currentDate = new Date();
    const startDateOfMonth = startOfMonth(currentDate);
    const formattedStartDate = format(startDateOfMonth, 'dd/MM/yyyy');
    const endDateOfMonth = endOfMonth(currentDate);
    const formattedEndDate = format(endDateOfMonth, 'dd/MM/yyyy');
    this.datedebut = formattedStartDate;
    this.datefin = formattedEndDate;

    //console.log(formattedStartDate);
    this.getInfoStatuData();
    this.getInfoData();

  }

  RechercheAujourdui() {

    this.titreSelect=`Aujourd'hui`;
    this.state= "Done";
    this.startDate = new Date();
    this.endDate = new Date();
    this.datedebut=format(this.startDate, 'dd/MM/yyyy');
    this.datefin= format(this.endDate, 'dd/MM/yyyy');

    this.getInfoStatuData();
    this.getInfoData();

    //console.log(this.data1, this.data22)
  }

  RechercheSemaine() {

    this.titreSelect=`Cette semaine`;
    this.state= "Done";

    this.startDate = new Date();
    this.endDate = new Date();
    this.startDate = subDays(this.startDate, 7);
    this.endDate = subDays(this.endDate, 1);

    this.datedebut=format(this.startDate, 'dd/MM/yyyy');
    this.datefin= format(this.endDate, 'dd/MM/yyyy');

    this.getInfoStatuData();
    this.getInfoData();

  }

  

  public choosedDate($event: any) {
    this.titreSelect = `Personaliser`;
    this.state = "Done";
  
    this.startDate = subDays(new Date(), 7);
    this.endDate = subDays(new Date(), 1);
  
    if ($event.startDate && $event.startDate.$d) {
      this.datedebut = format($event.startDate.$d, 'dd/MM/yyyy');
    }
  
    if ($event.endDate && $event.endDate.$d) {
      this.datefin = format($event.endDate.$d, 'dd/MM/yyyy');
    }
  
    this.getInfoStatuData();
    this.getInfoData();
  }
  

}
