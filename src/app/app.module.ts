import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToastrModule, ToastContainerModule } from 'ngx-toastr';
import { NgxUiLoaderModule, NgxUiLoaderConfig, PB_DIRECTION, SPINNER } from 'ngx-ui-loader';
import { TokenInterceptorInterceptor } from './services/token-interceptor.interceptor';
import { FullComponent } from './layouts/full/full.component';
import { HeaderComponent } from './layouts/full/header/header.component';
import { SharedModule } from './shared/shared.module';
import {NgxPaginationModule} from 'ngx-pagination';
import { TestpaieComponent } from './testpaie/testpaie.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';


const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  text:"Loading...",
  textColor:"#FFFFFF",
  textPosition:"center-center",
  pbColor:"#ff7900",
  bgsColor:"#ff7900",
  fgsColor:"#ff7900",
  fgsType: SPINNER.ballSpinClockwise,
  fgsSize:100,
  pbDirection:PB_DIRECTION.leftToRight,
  pbThickness:5

}


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    FullComponent,
    HeaderComponent,
    TestpaieComponent
  ],
  imports: [
  BrowserModule,
  HighchartsChartModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    HttpClientModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig),
    ToastContainerModule,
    BrowserAnimationsModule,
    NgxPaginationModule,
    NgxDaterangepickerMd.forRoot(),
    ToastrModule.forRoot({
      timeOut: 2000, //  seconds
      closeButton: true,
      progressBar: true,
      positionClass: 'toast-center-center',
    }),
  ],
  providers: [HttpClientModule,{provide:HTTP_INTERCEPTORS,useClass:TokenInterceptorInterceptor,multi:true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
