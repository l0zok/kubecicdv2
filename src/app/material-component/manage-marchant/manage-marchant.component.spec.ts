import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageMarchantComponent } from './manage-marchant.component';

describe('ManageMarchantComponent', () => {
  let component: ManageMarchantComponent;
  let fixture: ComponentFixture<ManageMarchantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageMarchantComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ManageMarchantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
