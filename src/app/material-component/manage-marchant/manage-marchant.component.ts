import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { marchant } from 'src/app/model/marchant.model';
import { MarchantService } from 'src/app/services/marchant.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { GlobalConstants } from 'src/app/shared/global-constants';

@Component({
  selector: 'app-manage-marchant',
  templateUrl: './manage-marchant.component.html',
  styleUrls: ['./manage-marchant.component.scss']
})
export class ManageMarchantComponent implements OnInit {
  @ViewChild('closebutton') closebutton: any;
  responseMessage:any;
  errorMessage! : string;
  marchantFilter: any={code:''};
  /*nouvelle pagination*/
  currentPage1: number = 1;
  itemsPerPage1: number = 10;
  totalItems1: any;
  totalPages1: any;
  itemsToDisplay1!: any[] ;

  itemsToDisplayType!: any[] ;
  itemsToDisplaySubsType!: any[] ;
  
  currentPageNumber1: any;
  maxVisiblePages: number = 5; // Nombre maximal de numéros de pagination à afficher
  ellipsisThreshold: number = 5; // A partir de quel numéro de page les points de suspension seront affichés
   /*nouvelle pagination*/

   //popup
   selectedRow: any;

   //user connect id
   userId:any;

   //marchant id
   marchantId:any;

   //Popup Add and update
   showadd!: boolean;
   showupdate!: boolean;
   marchantmodelobj:marchant=new marchant

   formValue!: FormGroup;

    //Active desactiv statut
    activeDesactive : any;
   //fin add and update

   //dynamic input
   //dynamicFields: any[] = [];
   marchandArr: any[] = [];
   empForm!: FormGroup;

   itemsToAccountSelect!: any[] ;

  constructor(private ngxService:NgxUiLoaderService,
    private marchantService:MarchantService,
    private router: Router,
    private formBuilder: FormBuilder,
    private toastrService: ToastrService,) { }

  ngOnInit(): void {
   this.formValue = this.formBuilder.group({
      libelle1:  new FormControl('',Validators.required),
      type: ['',Validators.required],
      subsType: ['',Validators.required],
      technicalAccount: new FormArray([
        new FormGroup({
          pin: new FormControl(''),
          msisdn: new FormControl(''),
        })
      ])

    })

    this.empForm = this.formBuilder.group({
      libelle:  ['',Validators.required],
      account: this.formBuilder.array([])
    });

    
    this.fetchData();
    //this.empForm.controls['country'].setValue(this.typesCompte[0].id);
  }



  typesCompte = [
    {
      id: 'illimix',
      name: 'illimix'
    },
    {
      id: 'operateur',
      name: 'Opérateur'
    },
    {
      id: 'marchant',
      name: 'Marchant'
    },
    {
      id: 'fibre',
      name: 'fibre'
    },
    {
      id: 'mobile',
      name: 'mobile'
    }
  ];

  sousTypesCompte = [
    {
      id: 'postpaid',
      name: 'postpaid'
    },
    {
      id: 'Prepaid;Hybrid',
      name: 'Prepaid;Hybrid'
    },
    {
      id: 'marchant',
      name: 'Marchant'
    }
  ];



  fetchData() {
    this.marchantService.getMarchants(this.currentPage1-1, this.itemsPerPage1).subscribe((response: any) => {
        this.totalItems1 = response.count;
        this.totalPages1 = Math.ceil(this.totalItems1 / this.itemsPerPage1);
        this.itemsToDisplay1 = response.datas;
        this.currentPageNumber1 = this.currentPage1;
        //console.log(response.datas.account)
      },(error:any)=>{
        //this.ngxService.stop();
        if(error.error?.message){
          this.responseMessage = error.error?.message;
        }else{
          this.responseMessage = GlobalConstants.genericError;
        }
        //this.snackbarService.openSnackBar(this.responseMessage,GlobalConstants.error);
      });
  }
  
  getPaginationNumbers(): (number | string)[] {
    const totalPages1 = Math.ceil(this.totalItems1 / this.itemsPerPage1);
    const currentPage1 = this.currentPageNumber1;
  
    if (totalPages1 <= this.maxVisiblePages) {
      return Array.from({ length: totalPages1 }, (_, i) => i + 1);
    } else {
      let startPage: number;
      let endPage: number;
  
      if (currentPage1 <= this.ellipsisThreshold) {
        startPage = 1;
        endPage = this.maxVisiblePages;
      } else if (currentPage1 + this.ellipsisThreshold > totalPages1) {
        startPage = totalPages1 - this.maxVisiblePages + 1;
        endPage = totalPages1;
      } else {
        startPage = currentPage1 - Math.floor((this.maxVisiblePages - 1) / 2);
        endPage = currentPage1 + Math.floor(this.maxVisiblePages / 2);
      }
  
      const paginationNumbers: (number | string)[] = [];
  
      if (startPage > 1) {
        paginationNumbers.push(1);
      }
  
      if (startPage > 2) {
        paginationNumbers.push('ellipsis');
      }
  
      for (let i = startPage; i <= endPage; i++) {
        paginationNumbers.push(i);
      }
  
      if (endPage < totalPages1 - 1) {
        paginationNumbers.push('ellipsis');
      }
  
      if (endPage < totalPages1) {
        paginationNumbers.push(totalPages1);
      }
  
      return paginationNumbers;
    }
  }

  previousPage() {
    if (this.currentPage1 > 1) {
      this.currentPage1--;
      this.fetchData();
    }
  }

  nextPage() {
    if (this.currentPage1 < this.totalPages1) {
      this.currentPage1++;
      this.fetchData();
      //console.log(this.fetchData());
    }
  }

  getPageNumbers(): number[] {
    const pageNumbers1 = [];
    for (let i = 1; i <= this.totalPages1; i++) {
      pageNumbers1.push(i);
    }
    return pageNumbers1;
  }

  isPopupOpen: boolean = false;

  goToPage(pageNumber1: number | string) {
    if (typeof pageNumber1 === 'number') {
      this.currentPage1 = pageNumber1;
      this.fetchData();
    }
  }

  openPopup(p: any) {
    this.empForm.reset();
    this.selectedRow = p;
    this.isPopupOpen = true;
    
  }

  closePopup() {
    this.empForm.reset();
    this.selectedRow = null;
    this.isPopupOpen = false;
    
  }

  // Form add and update
  add() {
    this.showadd = true;
    this.showupdate = false;
    this.empForm.reset();
  }

  edit(p:any) {
    this.showadd = false;
    this.showupdate = true;
    this.marchantmodelobj.id = p.id;


    this.formValue = new FormGroup({
      libelle: new FormControl(p.libelle),
      type: new FormControl(p.type),
      subsType: new FormControl(p.subsType),
      technicalAccount: new FormArray([])
    });

    //console.log(p.account?.technicalAccount);

    if (p.account && p.account.technicalAccount && p.account.technicalAccount.length) {
      for (let i = 0; i < p.account.technicalAccount.length; i++) {
        const control = this.formValue.get('technicalAccount') as FormArray;
        control.push(
          new FormGroup({
            pin: new FormControl(p.account.technicalAccount[i].pin),
            msisdn: new FormControl(p.account.technicalAccount[i].msisdn)
          })
        );
      }
    }
    
  }

//update on edit
 
  updateMarchant(){
    let data = JSON.stringify(this.empForm.value)
    this.userId = localStorage.getItem('userconnectinfo');
    //console.log(this.marchantmodelobj.id)
    this.marchantService.updateMarchant(data,this.userId,this.marchantmodelobj.id).subscribe((response:any)=>{
      this.ngxService.stop();
      this.responseMessage = response?.message;
      this.toastrService.success('Compte modifié avec succès!');
      this.empForm.reset();
      this.fetchData();
      this.closebutton.nativeElement.click();
    },(error:any)=>{
      //console.log(error);
      this.ngxService.stop();
      if(error.error?.message){
        this.responseMessage = error.error?.message;
      }else{
        this.responseMessage = GlobalConstants.genericError;
      }
    })

  }

  enableMarchant(){

      this.marchantService.enableMarchant(this.userId,this.marchantmodelobj.id).subscribe((response:any)=>{
        this.ngxService.stop();
        this.responseMessage = response?.message;
        this.toastrService.success('Compte activé avec succès!');
        this.formValue.reset();
        this.fetchData();
      },(error:any)=>{
        this.ngxService.stop();
        if(error.error?.message){
          this.responseMessage = error.error?.message;
        }else{
          this.responseMessage = GlobalConstants.genericError;
        }
      })

  }

  disableMarchant(){
    
      this.marchantService.disableMarchant(this.userId,this.marchantmodelobj.id).subscribe((response:any)=>{
        this.ngxService.stop();
        this.responseMessage = response?.message;
        this.toastrService.success('Compte désactivé avec succès!');
        this.formValue.reset();
        this.fetchData();
      },(error:any)=>{
        this.ngxService.stop();
        if(error.error?.message){
          this.responseMessage = error.error?.message;
        }else{
          this.responseMessage = GlobalConstants.genericError;
        }
      })

  }

  handleChangeAction(event: Event, id: number) {
    this.ngxService.start();
    const checkbox = event.target as HTMLInputElement;
    const isChecked = checkbox.checked;
    this.userId = localStorage.getItem('userconnectinfo');
    this.marchantmodelobj.id = id;
    
    if(isChecked){
      this.enableMarchant();
    }else{
      this.disableMarchant();
    }

  }

  resetData() {
    
  }

  editAccount(p: any) {    
    this.showadd = false;
    this.showupdate = true;
    this.marchantmodelobj.id = p.id;
    this.empForm.controls['libelle'].setValue(p.libelle);
    
    this.itemsToAccountSelect = p.account;
    
    const account: FormArray = this.empForm.get('account') as FormArray;
    account.clear(); // Supprimer les comptes existants avant de les ajouter à nouveau
    
    p.account.forEach((item: any) => {
      const technicalAccount: FormArray = this.formBuilder.array([]); // Créer un FormArray vide pour les comptes techniques
  
      item.technicalAccount.forEach((technicalItem: any) => {
        
        const accountSkills = this.formBuilder.group({
          pin: [technicalItem.pin, Validators.required],
          id: technicalItem.id,
          msisdn: [technicalItem.msisdn, Validators.required]
        });
        technicalAccount.push(accountSkills);
        
      });
  
      const newFormGroup = this.formBuilder.group({
        code: item.code,
        type: item.type,
        id: item.id,
        subsType: item.subsType,
        technicalAccount: technicalAccount
      });
  
      account.push(newFormGroup);
      
    });

  }

  
  editAccount11(p: any) {    
    this.showadd = false;
    this.showupdate = true;
    this.marchantmodelobj.id = p.id;
    this.empForm.controls['libelle'].setValue(p.libelle)
    this.itemsToAccountSelect=p.account;

    const account: FormArray = this.empForm.get('account') as FormArray;

    p.account.forEach((item: any, empIndex: number) => {
      //empIndex=1;
      const technicalAccount = item.technicalAccount;
      const accountSkills: FormArray = this.empForm.get('technicalAccount') as FormArray;

      //console.log(empIndex);

      item.technicalAccount.forEach((technicalItem: any) => {
        const accountSkills = this.formBuilder.group({
          pin: [technicalItem.pin, Validators.required],
          msisdn: [technicalItem.msisdn, Validators.required]
        });
        //console.log(accountSkills);
        technicalAccount.push(accountSkills);
      });

      const newFormGroup = this.formBuilder.group({
        type: item.type,
        subsType: item.subsType,
        technicalAccount: technicalAccount
      });

      account.push(newFormGroup);
    });

  }

  newAccount111(p:any): FormGroup {
    return this.formBuilder.group({
      type: p.type,
      subsType: p.subsType,
      technicalAccount: this.formBuilder.array([])
    });
  }
  newSkill1(p:any): FormGroup {
    return this.formBuilder.group({
      msisdn: ['',Validators.required],
      pin: ['',Validators.required]
    });
  }


  
  addAccount1(p:any) {
    this.account1(p).push(this.newAccount1(p));
  }

  account1(p:any): FormArray {
    return p;
  }

  newAccount1(p:any): FormGroup {
    return this.formBuilder.group({
      type: p.type,
      subsType: p.subsType,
      technicalAccount: this.formBuilder.array([])
    });
  }

  editAccountSkill(empIndex: number, skillIndex: number, updatedSkill: FormGroup) {
    this.accountSkills(empIndex).at(skillIndex).patchValue(updatedSkill.value);
  }

  account(): FormArray {
    return this.empForm.get('account') as FormArray;
  }
 
  newAccount(): FormGroup {
    return this.formBuilder.group({
      type: ['',Validators.required],
      subsType: ['',Validators.required],
      technicalAccount: this.formBuilder.array([])
    });
  }
 
  addAccount() {
    this.account().push(this.newAccount());
  }
 
  removeAccount(empIndex: number) {
    this.account().removeAt(empIndex);
  }
 
  accountSkills(empIndex: number): FormArray {
    return this.account()
      .at(empIndex)
      .get('technicalAccount') as FormArray;
  }
 
  newSkill(): FormGroup {
    return this.formBuilder.group({
      msisdn: ['',Validators.required],
      pin: ['',Validators.required]
    });
  }
 
  addAccountSkill(empIndex: number) {
    this.accountSkills(empIndex).push(this.newSkill());
  }
 
  removeAccountSkill(empIndex: number, skillIndex: number) {
    this.accountSkills(empIndex).removeAt(skillIndex);
  }
 
  onSubmit() {
    //console.log(this.empForm.value);
    let data = JSON.stringify(this.empForm.value)
    //console.log(data);
    this.userId = localStorage.getItem('userconnectinfo');
    
    this.marchantService.newMarchant(data,this.userId).subscribe((response:any)=>{
      this.ngxService.stop();
      this.responseMessage = response?.message;
      this.toastrService.success('Compte crée avec succès!');
      this.empForm.reset();
      this.fetchData();
      this.closebutton.nativeElement.click();
      //console.log(data);
    },(error:any)=>{
      //console.log(error);
      this.ngxService.stop();
      if(error.error?.message){
        this.responseMessage = error.error?.message;
      }else{
        this.responseMessage = GlobalConstants.genericError;
      }
    })
    
  }

}
