import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManageUserComponent } from './manage-user/manage-user.component';
import { RouteGuardService } from '../services/route-guard.service';
import { ManageTransfertComponent } from './manage-transfert/manage-transfert.component';
import { ManagePaiementComponent } from './manage-paiement/manage-paiement.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { DetailTransfertComponent } from './detail-transfert/detail-transfert.component';
import { DetailPaiementComponent } from './detail-paiement/detail-paiement.component';
import { ManageMarchantComponent } from './manage-marchant/manage-marchant.component';
import { ManageChannelComponent } from './manage-channel/manage-channel.component';
import { ManageRoleComponent } from './manage-role/manage-role.component';

export const MaterialRoutes: Routes = [
  {
    path: 'transfert',
    component: ManageTransfertComponent,
    /*canActivate: [RouteGuardService],
    data: {
      expectedRole: ['admin','user']
    }*/
  },
  {
    path: 'paiement',
    component: ManagePaiementComponent,
    /*canActivate: [RouteGuardService],
    data: {
      expectedRole: ['admin','user']
    }*/
  },
  {
    path: 'user',
    component: ManageUserComponent,
    /*canActivate: [RouteGuardService],
    data: {
      expectedRole: ['admin']
    }*/
  },
  {
    path: 'channel',
    component: ManageChannelComponent,
    /*canActivate: [RouteGuardService],
    data: {
      expectedRole: ['admin']
    }*/
  },
  {
    path: 'role',
    component: ManageRoleComponent,
    /*canActivate: [RouteGuardService],
    data: {
      expectedRole: ['admin']
    }*/
  },
  {
    path: 'marchant',
    component: ManageMarchantComponent,
    /*canActivate: [RouteGuardService],
    data: {
      expectedRole: ['admin']
    }*/
  },
  {
    path: 'edit-user/:id',
    component: EditUserComponent,
    /*canActivate: [RouteGuardService],
    data: {
      expectedRole: ['admin']
    }*/
  },
  {
    path: 'detail-transfert/:id',
    component: DetailTransfertComponent,
    /*canActivate: [RouteGuardService],
    data: {
      expectedRole: ['admin']
    }*/
  },
  {
    path: 'detail-paiement/:id',
    component: DetailPaiementComponent,
    /*canActivate: [RouteGuardService],
    data: {
      expectedRole: ['admin']
    }*/
  }
];

