import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { PaiementService } from 'src/app/services/paiement.service';
import { GlobalConstants } from 'src/app/shared/global-constants';

@Component({
  selector: 'app-detail-paiement',
  templateUrl: './detail-paiement.component.html',
  styleUrls: ['./detail-paiement.component.scss']
})
export class DetailPaiementComponent implements OnInit{
  detailPaiementForm:any = FormGroup;
  paiementId! : string;
  dataSource:any;
  responseMessage:any;

  constructor(private formBuilder:FormBuilder,
    private ngxService:NgxUiLoaderService,
    private paiementService:PaiementService,
    private router: ActivatedRoute
    ){
      this.paiementId = this.router.snapshot.params['id']
    }

  ngOnInit(): void {
    this.userData();
  }

  userData(){
    this.paiementService.getDetailPaiement(this.paiementId).subscribe((response:any)=>{
      this.ngxService.stop();
      this.dataSource = response;
      
      this.detailPaiementForm = this.formBuilder.group({
        paie_reference : this.formBuilder.control(this.dataSource.paie_reference),
        paie_code : this.formBuilder.control(this.dataSource.paie_code),
        paie_sender : this.formBuilder.control(this.dataSource.paie_sender),
        paie_receiver : this.formBuilder.control(this.dataSource.paie_receiver),
        paie_amount : this.formBuilder.control(this.dataSource.paie_amount),
        paie_taxamount : this.formBuilder.control(this.dataSource.paie_taxamount),
        paie_totalamount : this.formBuilder.control(this.dataSource.paie_totalamount),
        paie_create : this.formBuilder.control(this.dataSource.paie_create),
        paie_status : this.formBuilder.control(this.dataSource.paie_status),
      })
    },(error:any)=>{
      this.ngxService.stop();
      if(error.error?.message){
        this.responseMessage = error.error?.message;
      }else{
        this.responseMessage = GlobalConstants.genericError;
      }
    })
  }

}
