import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { TransfertService } from 'src/app/services/transfert.service';
import { GlobalConstants } from 'src/app/shared/global-constants';

@Component({
  selector: 'app-detail-transfert',
  templateUrl: './detail-transfert.component.html',
  styleUrls: ['./detail-transfert.component.scss']
})
export class DetailTransfertComponent {

  detailTransfertForm:any = FormGroup;
  transfertId! : string;
  dataSource:any;
  responseMessage:any;

  constructor(private formBuilder:FormBuilder,
    private ngxService:NgxUiLoaderService,
    private transfertService:TransfertService,
    private router: ActivatedRoute
    ){
      this.transfertId = this.router.snapshot.params['id']
    }

  ngOnInit(): void {
    this.userData();
  }

  userData(){
    this.transfertService.getDetailTransfert(this.transfertId).subscribe((response:any)=>{
      this.ngxService.stop();
      this.dataSource = response;
      
      this.detailTransfertForm = this.formBuilder.group({
        trans_reference : this.formBuilder.control(this.dataSource.trans_reference),
        trans_code : this.formBuilder.control(this.dataSource.trans_code),
        trans_sender : this.formBuilder.control(this.dataSource.trans_sender),
        trans_receiver : this.formBuilder.control(this.dataSource.trans_receiver),
        trans_amount : this.formBuilder.control(this.dataSource.trans_amount),
        trans_taxamount : this.formBuilder.control(this.dataSource.trans_taxamount),
        trans_totalamount : this.formBuilder.control(this.dataSource.trans_totalamount),
        trans_create : this.formBuilder.control(this.dataSource.trans_create),
        trans_status : this.formBuilder.control(this.dataSource.trans_status),
      })
    },(error:any)=>{
      this.ngxService.stop();
      if(error.error?.message){
        this.responseMessage = error.error?.message;
      }else{
        this.responseMessage = GlobalConstants.genericError;
      }
    })
  }

}
