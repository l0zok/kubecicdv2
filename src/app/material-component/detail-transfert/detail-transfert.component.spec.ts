import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailTransfertComponent } from './detail-transfert.component';

describe('DetailTransfertComponent', () => {
  let component: DetailTransfertComponent;
  let fixture: ComponentFixture<DetailTransfertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailTransfertComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DetailTransfertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
