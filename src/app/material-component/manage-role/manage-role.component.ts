import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { role } from 'src/app/model/role.model';
import { GlobalConstants } from 'src/app/shared/global-constants';
import { ToastrService } from 'ngx-toastr';
import { RoleService } from 'src/app/services/role.service';

@Component({
  selector: 'app-manage-role',
  templateUrl: './manage-role.component.html',
  styleUrls: ['./manage-role.component.scss']
})
export class ManageRoleComponent {
  @ViewChild('closebutton') closebutton: any;

  responseMessage:any;
  errorMessage! : string;
  roleFilter: any={name:''};
  /*nouvelle pagination*/
  currentPage1: number = 1;
  itemsPerPage1: number = 10;
  totalItems1: any;
  totalPages1: any;
  itemsToDisplay1!: any[] ;
  currentPageNumber1: any;
  maxVisiblePages: number = 5; // Nombre maximal de numéros de pagination à afficher
  ellipsisThreshold: number = 5; // A partir de quel numéro de page les points de suspension seront affichés
   /*nouvelle pagination*/

   //popup
   selectedRow: any;

   //user connect id
   userId:any;

   //role id
   roleId:any;

   //Popup Add and update
   showadd!: boolean;
   showupdate!: boolean;
   rolemodelobj:role=new role;

   formValue!: FormGroup;

    //Active desactiv statut
    activeDesactive : any;
   //fin add and update


  constructor(private ngxService:NgxUiLoaderService,
    private roleService:RoleService,
    private router: Router,
    private formBuilder: FormBuilder,
    private toastrService: ToastrService,) { }

  ngOnInit(): void {
    this.formValue = this.formBuilder.group({
      name:  ['',Validators.required],
      title: ['',Validators.required],
      descrption: ['',Validators.required],
    })
    this.fetchData();
  }

  fetchData() {
    this.roleService.getRoles(this.currentPage1-1, this.itemsPerPage1).subscribe((response: any) => {
        this.totalItems1 = response.count;
        this.totalPages1 = Math.ceil(this.totalItems1 / this.itemsPerPage1);
        this.itemsToDisplay1 = response.datas;
        this.currentPageNumber1 = this.currentPage1;
        //console.log(response.datas)
      },(error:any)=>{
        //this.ngxService.stop();
        if(error.error?.message){
          this.responseMessage = error.error?.message;
        }else{
          this.responseMessage = GlobalConstants.genericError;
        }
        //this.snackbarService.openSnackBar(this.responseMessage,GlobalConstants.error);
      });
  }
  
  getPaginationNumbers(): (number | string)[] {
    const totalPages1 = Math.ceil(this.totalItems1 / this.itemsPerPage1);
    const currentPage1 = this.currentPageNumber1;
  
    if (totalPages1 <= this.maxVisiblePages) {
      return Array.from({ length: totalPages1 }, (_, i) => i + 1);
    } else {
      let startPage: number;
      let endPage: number;
  
      if (currentPage1 <= this.ellipsisThreshold) {
        startPage = 1;
        endPage = this.maxVisiblePages;
      } else if (currentPage1 + this.ellipsisThreshold > totalPages1) {
        startPage = totalPages1 - this.maxVisiblePages + 1;
        endPage = totalPages1;
      } else {
        startPage = currentPage1 - Math.floor((this.maxVisiblePages - 1) / 2);
        endPage = currentPage1 + Math.floor(this.maxVisiblePages / 2);
      }
  
      const paginationNumbers: (number | string)[] = [];
  
      if (startPage > 1) {
        paginationNumbers.push(1);
      }
  
      if (startPage > 2) {
        paginationNumbers.push('ellipsis');
      }
  
      for (let i = startPage; i <= endPage; i++) {
        paginationNumbers.push(i);
      }
  
      if (endPage < totalPages1 - 1) {
        paginationNumbers.push('ellipsis');
      }
  
      if (endPage < totalPages1) {
        paginationNumbers.push(totalPages1);
      }
  
      return paginationNumbers;
    }
  }

  previousPage() {
    if (this.currentPage1 > 1) {
      this.currentPage1--;
      this.fetchData();
    }
  }

  nextPage() {
    if (this.currentPage1 < this.totalPages1) {
      this.currentPage1++;
      this.fetchData();
      //console.log(this.fetchData());
    }
  }

  getPageNumbers(): number[] {
    const pageNumbers1 = [];
    for (let i = 1; i <= this.totalPages1; i++) {
      pageNumbers1.push(i);
    }
    return pageNumbers1;
  }

  isPopupOpen: boolean = false;

  goToPage(pageNumber1: number | string) {
    if (typeof pageNumber1 === 'number') {
      this.currentPage1 = pageNumber1;
      this.fetchData();
    }
  }

  openPopup(p: any) {
    this.selectedRow = p;
    this.isPopupOpen = true;
  }

  closePopup() {
    this.selectedRow = null;
    this.isPopupOpen = false;
  }

  // Form add and update
  add() {
    this.showadd = true;
    this.showupdate = false;
    this.formValue.reset();
  }

  edit(p:any) {
    this.showadd = false;
    this.showupdate = true;
    this.rolemodelobj.id = p.id;
    this.formValue.controls['name'].setValue(p.name)
    this.formValue.controls['title'].setValue(p.title)
    this.formValue.controls['descrption'].setValue(p.descrption)
  }

//update on edit
  updateRole(){
    this.rolemodelobj.name = this.formValue.value.name;
    this.rolemodelobj.title= this.formValue.value.title;
    this.rolemodelobj.descrption=this.formValue.value.descrption;
    this.userId = localStorage.getItem('userconnectinfo');

      this.roleService.updateRole(this.rolemodelobj,this.userId,this.rolemodelobj.id).subscribe((response:any)=>{
        this.ngxService.stop();
        this.responseMessage = response?.message;
        this.toastrService.success('Compte modifié avec succès!');
        this.formValue.reset();
        this.fetchData();
        this.closebutton.nativeElement.click();
      },(error:any)=>{
        this.ngxService.stop();
        if(error.error?.message){
          this.responseMessage = error.error?.message;
        }else{
          this.responseMessage = GlobalConstants.genericError;
        }
      })

  }

  addRole(){
    this.rolemodelobj.name = this.formValue.value.name;
    this.rolemodelobj.title= this.formValue.value.title;
    this.rolemodelobj.descrption=this.formValue.value.descrption;
    this.userId = localStorage.getItem('userconnectinfo');

    this.roleService.newRole(this.rolemodelobj,this.userId).subscribe((response:any)=>{
      this.ngxService.stop();
      this.responseMessage = response?.message;
      this.toastrService.success('Compte crée avec succès!');
      this.formValue.reset();
      this.fetchData();
      this.closebutton.nativeElement.click();
    },(error:any)=>{
      this.ngxService.stop();
      if(error.error?.message){
        this.responseMessage = error.error?.message;
      }else{
        this.responseMessage = GlobalConstants.genericError;
      }
    })

  }

  enableRole(){

      this.roleService.enableRole(this.userId,this.rolemodelobj.id).subscribe((response:any)=>{
        this.ngxService.stop();
        this.responseMessage = response?.message;
        this.toastrService.success('Compte activé avec succès!');
        this.formValue.reset();
        this.fetchData();
      },(error:any)=>{
        this.ngxService.stop();
        if(error.error?.message){
          this.responseMessage = error.error?.message;
        }else{
          this.responseMessage = GlobalConstants.genericError;
        }
      })

  }

  disableRole(){
    
      this.roleService.disableRole(this.userId,this.rolemodelobj.id).subscribe((response:any)=>{
        this.ngxService.stop();
        this.responseMessage = response?.message;
        this.toastrService.success('Compte désactivé avec succès!');
        this.formValue.reset();
        this.fetchData();
      },(error:any)=>{
        this.ngxService.stop();
        if(error.error?.message){
          this.responseMessage = error.error?.message;
        }else{
          this.responseMessage = GlobalConstants.genericError;
        }
      })

  }

  handleChangeAction(event: Event, id: number) {
    this.ngxService.start();
    const checkbox = event.target as HTMLInputElement;
    const isChecked = checkbox.checked;
    this.userId = localStorage.getItem('userconnectinfo');
    this.rolemodelobj.id = id;
    
    if(isChecked){
      this.enableRole();
    }else{
      this.disableRole();
    }

  }

  resetData() {
    
  }

}
