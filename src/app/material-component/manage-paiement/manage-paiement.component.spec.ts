import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagePaiementComponent } from './manage-paiement.component';

describe('ManagePaiementComponent', () => {
  let component: ManagePaiementComponent;
  let fixture: ComponentFixture<ManagePaiementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagePaiementComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ManagePaiementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
