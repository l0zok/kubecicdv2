import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { PaiementService } from 'src/app/services/paiement.service';
import { GlobalConstants } from 'src/app/shared/global-constants';
import { format, startOfMonth, endOfMonth, subDays } from 'date-fns';

@Component({
  selector: 'app-manage-paiement',
  templateUrl: './manage-paiement.component.html',
  styleUrls: ['./manage-paiement.component.scss']
})
export class ManagePaiementComponent implements OnInit{
  @ViewChild('closebutton') closebutton: any;
  dataSource:any;
  responseMessage:any;
  errorMessage! : string;
  paiementFilter: any={account:''};

  currentPage : number=1;
  pageSize : number=10;
  u: number = 1;
  totalPaiement:any;
  state: any = "";

  datedebut!: string;
  datefin!: string;

  formValue!: FormGroup;

  /*nouvelle pagination*/
  currentPage1: number = 1;
  itemsPerPage1: number = 10;
  totalItems1: any;
  totalPages1: any;
  itemsToDisplay1!: any[] ;
  currentPageNumber1: any;
  maxVisiblePages: number = 5; // Nombre maximal de numéros de pagination à afficher
  ellipsisThreshold: number = 5; // A partir de quel numéro de page les points de suspension seront affichés
   /*nouvelle pagination*/

   //popup
   selectedRow: any;

   //Fin popup

  constructor(private ngxService:NgxUiLoaderService,
    private paiementService:PaiementService,
    private formBuilder: FormBuilder,
    private router: Router) { }

    ngOnInit(): void {
      //this.ngxService.start();
      //this.tableData();
      this.fetchData();
      this.formValue = this.formBuilder.group({
        startDate:  [''],
        endDate: [''],
        state: [''],
      })
    }

    tableData(){
      this.paiementService.getPaiements().subscribe((response:any)=>{
        //this.ngxService.stop();
       
        this.dataSource = response.datas;
        this.totalPaiement = response.count;
        //console.log(this.totalPaiement);
      },(error:any)=>{
        //this.ngxService.stop();
        if(error.error?.message){
          this.responseMessage = error.error?.message;
        }else{
          this.responseMessage = GlobalConstants.genericError;
        }
        //this.snackbarService.openSnackBar(this.responseMessage,GlobalConstants.error);
      })
    }

    handleDetailPaiement(id: string){
      this.router.navigateByUrl("/arkham/detail-paiement/"+id);
    }


    fetchData() {
      this.paiementService.getApiUrl(this.currentPage1-1, this.itemsPerPage1).subscribe((response: any) => {
          this.totalItems1 = response.count;
          this.totalPages1 = Math.ceil(this.totalItems1 / this.itemsPerPage1);
          this.itemsToDisplay1 = response.datas;
          this.currentPageNumber1 = this.currentPage1;
          //console.log(this.currentPageNumber1)
        },(error:any)=>{
          //this.ngxService.stop();
          if(error.error?.message){
            this.responseMessage = error.error?.message;
          }else{
            this.responseMessage = GlobalConstants.genericError;
          }
          //this.snackbarService.openSnackBar(this.responseMessage,GlobalConstants.error);
        });
    }
    
    getPaginationNumbers(): (number | string)[] {
      const totalPages1 = Math.ceil(this.totalItems1 / this.itemsPerPage1);
      const currentPage1 = this.currentPageNumber1;
    
      if (totalPages1 <= this.maxVisiblePages) {
        return Array.from({ length: totalPages1 }, (_, i) => i + 1);
      } else {
        let startPage: number;
        let endPage: number;
    
        if (currentPage1 <= this.ellipsisThreshold) {
          startPage = 1;
          endPage = this.maxVisiblePages;
        } else if (currentPage1 + this.ellipsisThreshold > totalPages1) {
          startPage = totalPages1 - this.maxVisiblePages + 1;
          endPage = totalPages1;
        } else {
          startPage = currentPage1 - Math.floor((this.maxVisiblePages - 1) / 2);
          endPage = currentPage1 + Math.floor(this.maxVisiblePages / 2);
        }
    
        const paginationNumbers: (number | string)[] = [];
    
        if (startPage > 1) {
          paginationNumbers.push(1);
        }
    
        if (startPage > 2) {
          paginationNumbers.push('ellipsis');
        }
    
        for (let i = startPage; i <= endPage; i++) {
          paginationNumbers.push(i);
        }
    
        if (endPage < totalPages1 - 1) {
          paginationNumbers.push('ellipsis');
        }
    
        if (endPage < totalPages1) {
          paginationNumbers.push(totalPages1);
        }
    
        return paginationNumbers;
      }
    }

    previousPage() {
      if (this.currentPage1 > 1) {
        this.currentPage1--;
        this.fetchData();
      }
    }
  
    nextPage() {
      if (this.currentPage1 < this.totalPages1) {
        this.currentPage1++;
        this.fetchData();
        //console.log(this.fetchData());
      }
    }
  
    getPageNumbers(): number[] {
      const pageNumbers1 = [];
      for (let i = 1; i <= this.totalPages1; i++) {
        pageNumbers1.push(i);
      }
      return pageNumbers1;
    }
  
    /*goToPage(pageNumber1: number | string) {
      if (pageNumber1 >= 1 && pageNumber1 <= this.totalPages1) {
        this.currentPage1 = pageNumber1;
        this.fetchData();
      }
    }*/

    isPopupOpen: boolean = false;

    goToPage(pageNumber1: number | string) {
      if (typeof pageNumber1 === 'number') {
        this.currentPage1 = pageNumber1;
        this.fetchData();
      }
    }

    openPopup(row: any) {
      this.selectedRow = row;
      this.isPopupOpen = true;
    }
  
    closePopup() {
      this.selectedRow = null;
      this.isPopupOpen = false;
    }

    Recherche() {
      const startDateValue = this.formValue.value.startDate;
      const endDateValue = this.formValue.value.endDate;
      this.state = this.formValue.value.state;
  
      // Vérifier si les dates sont valides
      if (startDateValue && endDateValue) {
        // Convertir les valeurs en objets de date
        const startDate = new Date(startDateValue);
        const endDate = new Date(endDateValue);
        //this.state = this.formValue.value.state;
    
        // Vérifier si les dates sont valides avant de les formater
        if (!isNaN(startDate.getTime()) && !isNaN(endDate.getTime())) {
          
          this.datedebut = format(startDate, 'dd/MM/yyyy');
          this.datefin = format(endDate, 'dd/MM/yyyy');

          this.paiementService.SearchgetApiUrl(this.currentPage1-1, this.itemsPerPage1, this.datedebut, this.datefin).subscribe((response: any) => {
            this.totalItems1 = response.count;
            this.totalPages1 = Math.ceil(this.totalItems1 / this.itemsPerPage1);
            this.itemsToDisplay1 = response.datas;
            this.currentPageNumber1 = this.currentPage1;
            console.log(response)
          },(error:any)=>{
            //this.ngxService.stop();
            if(error.error?.message){
              this.responseMessage = error.error?.message;
            }else{
              this.responseMessage = GlobalConstants.genericError;
            }
            //this.snackbarService.openSnackBar(this.responseMessage,GlobalConstants.error);
          });

          /*this.startDate = startDate;
          this.endDate = endDate;
    
          this.datedebut = format(this.startDate, 'dd/MM/yyyy');
          this.datefin = format(this.endDate, 'dd/MM/yyyy');
          this.getInfoStatuData();
          this.getInfoData();*/
          console.log(this.datedebut, this.datefin)
          this.closebutton.nativeElement.click();
          //console.log(this.datedebut);
        } else {
          console.log('Dates invalides');
        }
      }
    }

    resetData(){
      this.fetchData();
    }

}
