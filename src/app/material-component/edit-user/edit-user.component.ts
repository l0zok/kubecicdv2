import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { UserService } from 'src/app/services/user.service';
import { GlobalConstants } from 'src/app/shared/global-constants';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  editUserForm:any = FormGroup;
  userId! : string;
  dataSource:any;
  responseMessage:any;

  constructor(private formBuilder:FormBuilder,
    private ngxService:NgxUiLoaderService,
    private userService:UserService,
    private snackbarService:SnackbarService,
    private toastrService: ToastrService,
    private router: ActivatedRoute,
    private route: Router
    ){
      this.userId = this.router.snapshot.params['id']
    }

  ngOnInit(): void {
    this.userData();
  }

  userData(){
    this.userService.getUser(this.userId).subscribe((response:any)=>{
      this.ngxService.stop();
      this.dataSource = response;
      this.editUserForm = this.formBuilder.group({
        name : this.formBuilder.control(this.dataSource.name),
        contactNumber : this.formBuilder.control(this.dataSource.contactNumber),
        email : this.formBuilder.control(this.dataSource.email),
        role : this.formBuilder.control(this.dataSource.role),
      })
    },(error:any)=>{
      this.ngxService.stop();
      if(error.error?.message){
        this.responseMessage = error.error?.message;
      }else{
        this.responseMessage = GlobalConstants.genericError;
      }
      //this.snackbarService.openSnackBar(this.responseMessage,GlobalConstants.error);
    })
  }

  /*handleUpdateSubmit(){
    this.ngxService.start();
    var formData = this.editUserForm.value;
    var statut = "active"
    var data = {
      name:formData.name,
      contactNumber:formData.contactNumber,
      email:formData.email,
      role:formData.role,
      id:this.userId,
    }
    this.userService.updateUser(data).subscribe((response:any)=>{
      this.ngxService.stop();
      this.responseMessage = response?.message;
      this.toastrService.success('Compte modifié avec succès!');
      this.editUserForm.reset();
      this.route.navigateByUrl("/arkham/user");
    },(error:any)=>{
      this.ngxService.stop();
      if(error.error?.message){
        this.responseMessage = error.error?.message;
      }else{
        this.responseMessage = GlobalConstants.genericError;
      }
    })
  }
  */
  handleUpdateProduct(){
    let u=this.editUserForm.value;
    u.id = this.dataSource.id
    alert(this.userId);
  }

}
