import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { ManageUserComponent } from './manage-user/manage-user.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialRoutes } from './material.routing';
import { ManageTransfertComponent } from './manage-transfert/manage-transfert.component';
import { ManagePaiementComponent } from './manage-paiement/manage-paiement.component';
import { NewUserComponent } from './new-user/new-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { DetailUserComponent } from './detail-user/detail-user.component';
import { DetailTransfertComponent } from './detail-transfert/detail-transfert.component';
import { DetailPaiementComponent } from './detail-paiement/detail-paiement.component';
import { NewMarchantComponent } from './new-marchant/new-marchant.component';
import { ManageMarchantComponent } from './manage-marchant/manage-marchant.component';
import { ManageChannelComponent } from './manage-channel/manage-channel.component';
import { ManageRoleComponent } from './manage-role/manage-role.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(MaterialRoutes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    FilterPipeModule,
  ],
  providers: [],
  declarations: [
    ManageUserComponent,
    ManageTransfertComponent,
    ManagePaiementComponent,
    NewUserComponent,
    EditUserComponent,
    DetailUserComponent,
    DetailTransfertComponent,
    DetailPaiementComponent,
    NewMarchantComponent,
    ManageMarchantComponent,
    ManageChannelComponent,
    ManageRoleComponent    
  ]
})
export class MaterialComponentsModule { }
