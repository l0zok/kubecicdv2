import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { user } from 'src/app/model/user.model';
import { GlobalConstants } from 'src/app/shared/global-constants';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-manage-user',
  templateUrl: './manage-user.component.html',
  styleUrls: ['./manage-user.component.scss']
})
export class ManageUserComponent implements OnInit{
  @ViewChild('closebutton') closebutton: any;
  responseMessage:any;
  errorMessage! : string;
  userFilter: any={firstName:''};
  /*nouvelle pagination*/
  currentPage1: number = 1;
  itemsPerPage1: number = 10;
  totalItems1: any;
  totalPages1: any;
  itemsToDisplay1!: any[] ;
  itemsToDisplayRole!: any[] ;
  itemsToDisplayRoleSelect!: any[] ;
  itemsTolistRole!: any[] ;
  currentPageNumber1: any;
  maxVisiblePages: number = 5; // Nombre maximal de numéros de pagination à afficher
  ellipsisThreshold: number = 5; // A partir de quel numéro de page les points de suspension seront affichés
   /*nouvelle pagination*/

   //popup
   selectedRow: any;

   //user connect id
   userId:any;

   //poup
   showadd!: boolean;
   showupdate!: boolean;
   //openPop: boolean = false;
   usermodelobj:user=new user;

   formValue!: FormGroup;

    //Active desactiv statut
    activeDesactive : any;
    
  constructor(private ngxService:NgxUiLoaderService,
    private userService:UserService,
    private router: Router,
    private formBuilder: FormBuilder,
    private toastrService: ToastrService,) { }

  ngOnInit(): void {
    this.formValue = this.formBuilder.group({
      firstName:  ['',Validators.required],
      lastName: ['',Validators.required],
      email:  ['',Validators.required],
      contact: ['',Validators.required],
      username: ['',Validators.required],
      datasRole: this.formBuilder.array([])
    })

    this.fetchData();
    this.fetchDataRole();
  }

  fetchData() {
    this.userService.getUsers(this.currentPage1-1, this.itemsPerPage1).subscribe((response: any) => {
        this.totalItems1 = response.count;
        this.totalPages1 = Math.ceil(this.totalItems1 / this.itemsPerPage1);
        this.itemsToDisplay1 = response.datas;
        this.currentPageNumber1 = this.currentPage1;
        //console.log(response.datas)
      },(error:any)=>{
        //this.ngxService.stop();
        if(error.error?.message){
          this.responseMessage = error.error?.message;
        }else{
          this.responseMessage = GlobalConstants.genericError;
        }
        //this.snackbarService.openSnackBar(this.responseMessage,GlobalConstants.error);
      });
  }
  
  getPaginationNumbers(): (number | string)[] {
    const totalPages1 = Math.ceil(this.totalItems1 / this.itemsPerPage1);
    const currentPage1 = this.currentPageNumber1;
  
    if (totalPages1 <= this.maxVisiblePages) {
      return Array.from({ length: totalPages1 }, (_, i) => i + 1);
    } else {
      let startPage: number;
      let endPage: number;
  
      if (currentPage1 <= this.ellipsisThreshold) {
        startPage = 1;
        endPage = this.maxVisiblePages;
      } else if (currentPage1 + this.ellipsisThreshold > totalPages1) {
        startPage = totalPages1 - this.maxVisiblePages + 1;
        endPage = totalPages1;
      } else {
        startPage = currentPage1 - Math.floor((this.maxVisiblePages - 1) / 2);
        endPage = currentPage1 + Math.floor(this.maxVisiblePages / 2);
      }
  
      const paginationNumbers: (number | string)[] = [];
  
      if (startPage > 1) {
        paginationNumbers.push(1);
      }
  
      if (startPage > 2) {
        paginationNumbers.push('ellipsis');
      }
  
      for (let i = startPage; i <= endPage; i++) {
        paginationNumbers.push(i);
      }
  
      if (endPage < totalPages1 - 1) {
        paginationNumbers.push('ellipsis');
      }
  
      if (endPage < totalPages1) {
        paginationNumbers.push(totalPages1);
      }
  
      return paginationNumbers;
    }
  }

  previousPage() {
    if (this.currentPage1 > 1) {
      this.currentPage1--;
      this.fetchData();
    }
  }

  nextPage() {
    if (this.currentPage1 < this.totalPages1) {
      this.currentPage1++;
      this.fetchData();
      //console.log(this.fetchData());
    }
  }

  getPageNumbers(): number[] {
    const pageNumbers1 = [];
    for (let i = 1; i <= this.totalPages1; i++) {
      pageNumbers1.push(i);
    }
    return pageNumbers1;
  }

  isPopupOpen: boolean = false;

  goToPage(pageNumber1: number | string) {
    if (typeof pageNumber1 === 'number') {
      this.currentPage1 = pageNumber1;
      this.fetchData();
    }
  }

  openPopup(p: any) {
    this.selectedRow = p;
    this.isPopupOpen = true;
  }

  closePopup() {
    this.selectedRow = null;
    this.isPopupOpen = false;
  }

  // Form add and update
  add() {
    this.showadd = true;
    //this.openPop = true;
    this.showupdate = false;
    this.formValue.reset();
    this.clearFormArray();
    this.itemsToDisplayRole = this.itemsTolistRole.map((item) => ({
      ...item,
      selected: false
    }));
  }

  edit(p:any) {
    this.showadd = false;
    this.showupdate = true;
    this.usermodelobj.id = p.id;
    this.formValue.controls['firstName'].setValue(p.firstName)
    this.formValue.controls['lastName'].setValue(p.lastName)
    this.formValue.controls['email'].setValue(p.email)
    this.formValue.controls['contact'].setValue(p.contact)
    this.formValue.controls['username'].setValue(p.username)
   
    this.itemsToDisplayRoleSelect=p.roles;
   
    this.clearFormArray();
    const datasRole: FormArray = this.formValue.get('datasRole') as FormArray;
    p.roles.forEach((item: any) => {
      datasRole.push(new FormControl({ 'id': item.id }));
    });

    let useRoles = this.itemsTolistRole;
    let useRoleSelect = p.roles;
    useRoleSelect = useRoleSelect.map((item: any) => ({
      ...item,
      selected: true
    }));

    useRoles = useRoles.map((item: any) => ({
      ...item,
      selected: false
    }));

    this.itemsToDisplayRole = useRoles.map((checkbox) => {
      const correspondingRole = useRoleSelect.find((item: { id: any; }) => item.id === checkbox.id);
      if (correspondingRole) {
        checkbox.selected = correspondingRole.selected;
      }
      return checkbox;
    });


  }

//update on edit
  updateUser(){
    let data = JSON.stringify(this.formValue.value)
    this.userId = localStorage.getItem('userconnectinfo');
    this.userService.updateUser(data,this.userId,this.usermodelobj.id).subscribe((response:any)=>{
      this.ngxService.stop();
      this.responseMessage = response?.message;
      this.toastrService.success('Compte modifié avec succès!');
      this.formValue.reset();
      this.fetchData();
      this.closebutton.nativeElement.click();
    },(error:any)=>{
      //console.log(error);
      this.ngxService.stop();
      if(error.error?.message){
        this.responseMessage = error.error?.message;
      }else{
        this.responseMessage = GlobalConstants.genericError;
      }
    })

  }


  onSubmit() {
    let data = JSON.stringify(this.formValue.value)
    //console.log(data);
    this.userId = localStorage.getItem('userconnectinfo');
    this.userService.newUser(data,this.userId).subscribe((response:any)=>{
      this.ngxService.stop();
      this.responseMessage = response?.message;
      this.toastrService.success('Compte crée avec succès!');
      this.formValue.reset();
      this.fetchData();
      this.closebutton.nativeElement.click();
    },(error:any)=>{
      //console.log(error);
      this.ngxService.stop();
      if(error.error?.message){
        this.responseMessage = error.error?.message;
      }else{
        this.responseMessage = GlobalConstants.genericError;
      }
    })
    
  }

  enableUser(){

      this.userService.enableUser(this.userId,this.usermodelobj.id).subscribe((response:any)=>{
        this.ngxService.stop();
        this.responseMessage = response?.message;
        this.toastrService.success('Compte activé avec succès!');
        this.formValue.reset();
        this.fetchData();
      },(error:any)=>{
        this.ngxService.stop();
        if(error.error?.message){
          this.responseMessage = error.error?.message;
        }else{
          this.responseMessage = GlobalConstants.genericError;
        }
      })

  }

  disableUser(){
    
      this.userService.disableUser(this.userId,this.usermodelobj.id).subscribe((response:any)=>{
        this.ngxService.stop();
        this.responseMessage = response?.message;
        this.toastrService.success('Compte désactivé avec succès!');
        this.formValue.reset();
        this.fetchData();
      },(error:any)=>{
        this.ngxService.stop();
        if(error.error?.message){
          this.responseMessage = error.error?.message;
        }else{
          this.responseMessage = GlobalConstants.genericError;
        }
      })

  }

  handleChangeAction(event: Event, id: number) {
    this.ngxService.start();
    const checkbox = event.target as HTMLInputElement;
    const isChecked = checkbox.checked;
    this.userId = localStorage.getItem('userconnectinfo');
    this.usermodelobj.id = id;
    
    if(isChecked){
      this.enableUser();
    }else{
      this.disableUser();
    }

  }

  resetData() {
    
  }

  datasRole(): FormArray {
    return this.formValue.get('datasRole') as FormArray;
  }
  
 
  fetchDataRole() {
    this.userService.getRoles().subscribe((response: any) => {
        this.itemsTolistRole = response.datas;
        this.itemsToDisplayRole = response.datas;
        this.itemsToDisplayRole = this.itemsTolistRole.map((item) => ({
          ...item,
          selected: false
        }));
        //console.log(this.itemsToDisplayRole)
      },(error:any)=>{
        //this.ngxService.stop();
        if(error.error?.message){
          this.responseMessage = error.error?.message;
        }else{
          this.responseMessage = GlobalConstants.genericError;
        }
        //this.snackbarService.openSnackBar(this.responseMessage,GlobalConstants.error);
      });
  }
 
  clearFormArray() {
    //Angular 8 +
    this.datasRole().clear();
   
  }

  onCheckboxChange(e:any) {
    const datasRole: FormArray = this.formValue.get('datasRole') as FormArray;
   
    if (e.target.checked) {
      //datasRole.push(new FormControl(e.target.value));
      //datasRole.push(new FormControl({ 'id': e.target.value }));
      datasRole.push(new FormControl({ "id": Number(e.target.value) }));
    } else {
       const index = datasRole.controls.findIndex(x => x.value === e.target.value);
       datasRole.removeAt(index);
    }
  }

  
}
