import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { UserService } from 'src/app/services/user.service';
import { GlobalConstants } from 'src/app/shared/global-constants';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent implements OnInit {

  newUserForm:any = FormGroup;
  responseMessage:any;

  constructor(private formBuilder:FormBuilder,
    private ngxService:NgxUiLoaderService,
    private userService:UserService,
    private snackbarService:SnackbarService,
    private toastrService: ToastrService,
    private router: Router){}

  ngOnInit(): void {
    this.newUserForm = this.formBuilder.group({
      name:[null,[Validators.required]],
      contactNumber:[null,[Validators.required]],
      email:[null,[Validators.required]],
      role:[null,[Validators.required]],
      password:[null,[Validators.required]],
      confirmPassword:[null,[Validators.required]],
    })
  }

  validateSubmit(){
    if(this.newUserForm.controls['password'].value != this.newUserForm.controls['confirmPassword'].value){
      return true;
    }
    else{
      return false;
    }
  }

  /*handleSubmit(){
    this.ngxService.start();
    var formData = this.newUserForm.value;
    var statut = "active"
    var data = {
      name:formData.name,
      contactNumber:formData.contactNumber,
      email:formData.email,
      username:formData.email,
      password:formData.password,
      status:statut,
      confirmPassword:formData.confirmPassword,
      role:formData.role,
    }
    this.userService.newUser(data).subscribe((response:any)=>{
      this.ngxService.stop();
      this.responseMessage = response?.message;
      this.toastrService.success('Compte crée avec succès!');
      this.newUserForm.reset();
      this.router.navigateByUrl("/arkham/user");
    },(error:any)=>{
      this.ngxService.stop();
      if(error.error?.message){
        this.responseMessage = error.error?.message;
      }else{
        this.responseMessage = GlobalConstants.genericError;
      }
    })
  }*/

  
}
