import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewMarchantComponent } from './new-marchant.component';

describe('NewMarchantComponent', () => {
  let component: NewMarchantComponent;
  let fixture: ComponentFixture<NewMarchantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewMarchantComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewMarchantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
