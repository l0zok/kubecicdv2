import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { TransfertService } from 'src/app/services/transfert.service';
import { GlobalConstants } from 'src/app/shared/global-constants';

@Component({
  selector: 'app-manage-transfert',
  templateUrl: './manage-transfert.component.html',
  styleUrls: ['./manage-transfert.component.scss']
})
export class ManageTransfertComponent implements OnInit{

  dataSource:any;
  responseMessage:any;
  errorMessage! : string;
  transfertFilter: any={reference:''};

  currentPage : number=1;
  pageSize : number=10;
  u: number = 1;
  totalTransfert:any;

  /*nouvelle pagination*/
  currentPage1: number = 1;
  itemsPerPage1: number = 10;
  totalItems1: any;
  totalPages1: any;
  itemsToDisplay1!: any[] ;
  currentPageNumber1: any;
  maxVisiblePages: number = 5; // Nombre maximal de numéros de pagination à afficher
  ellipsisThreshold: number = 5; // A partir de quel numéro de page les points de suspension seront affichés
   /*nouvelle pagination*/

   //popup
   selectedRow: any;

   //Fin popup
   formValue!: FormGroup;


  constructor(private ngxService:NgxUiLoaderService,
    private transfertService:TransfertService,
    private formBuilder: FormBuilder,
    private router: Router) { }

    ngOnInit(): void {
      //this.ngxService.start();
      //this.tableData();
      this.fetchData();
      this.formValue = this.formBuilder.group({
        startDate:  [''],
        endDate: [''],
        state: [''],
      })
    }

    tableData(){
      this.transfertService.getTransferts().subscribe((response:any)=>{
        //this.ngxService.stop();
       
        this.dataSource = response.datas;
        this.totalTransfert = response.count;
        //console.log(this.totalTransfert);
      },(error:any)=>{
        //this.ngxService.stop();
        if(error.error?.message){
          this.responseMessage = error.error?.message;
        }else{
          this.responseMessage = GlobalConstants.genericError;
        }
        //this.snackbarService.openSnackBar(this.responseMessage,GlobalConstants.error);
      })
    }

    handleDetailTransfert(id: string){
      this.router.navigateByUrl("/arkham/detail-transfert/"+id);
    }


    fetchData() {
      this.transfertService.getApiUrl(this.currentPage1-1, this.itemsPerPage1).subscribe((response: any) => {
          this.totalItems1 = response.totalElement;
          this.totalPages1 = Math.ceil(this.totalItems1 / this.itemsPerPage1);
          this.itemsToDisplay1 = response.data;
          this.currentPageNumber1 = this.currentPage1;
          //console.log(this.itemsToDisplay1)
        },(error:any)=>{
          //this.ngxService.stop();
          if(error.error?.message){
            this.responseMessage = error.error?.message;
          }else{
            this.responseMessage = GlobalConstants.genericError;
          }
          //this.snackbarService.openSnackBar(this.responseMessage,GlobalConstants.error);
        });
    }
    
    getPaginationNumbers(): (number | string)[] {
      const totalPages1 = Math.ceil(this.totalItems1 / this.itemsPerPage1);
      const currentPage1 = this.currentPageNumber1;
    
      if (totalPages1 <= this.maxVisiblePages) {
        return Array.from({ length: totalPages1 }, (_, i) => i + 1);
      } else {
        let startPage: number;
        let endPage: number;
    
        if (currentPage1 <= this.ellipsisThreshold) {
          startPage = 1;
          endPage = this.maxVisiblePages;
        } else if (currentPage1 + this.ellipsisThreshold > totalPages1) {
          startPage = totalPages1 - this.maxVisiblePages + 1;
          endPage = totalPages1;
        } else {
          startPage = currentPage1 - Math.floor((this.maxVisiblePages - 1) / 2);
          endPage = currentPage1 + Math.floor(this.maxVisiblePages / 2);
        }
    
        const paginationNumbers: (number | string)[] = [];
    
        if (startPage > 1) {
          paginationNumbers.push(1);
        }
    
        if (startPage > 2) {
          paginationNumbers.push('ellipsis');
        }
    
        for (let i = startPage; i <= endPage; i++) {
          paginationNumbers.push(i);
        }
    
        if (endPage < totalPages1 - 1) {
          paginationNumbers.push('ellipsis');
        }
    
        if (endPage < totalPages1) {
          paginationNumbers.push(totalPages1);
        }
    
        return paginationNumbers;
      }
    }

    previousPage() {
      if (this.currentPage1 > 1) {
        this.currentPage1--;
        this.fetchData();
      }
    }
  
    nextPage() {
      if (this.currentPage1 < this.totalPages1) {
        this.currentPage1++;
        this.fetchData();
        //console.log(this.fetchData());
      }
    }
  
    getPageNumbers(): number[] {
      const pageNumbers1 = [];
      for (let i = 1; i <= this.totalPages1; i++) {
        pageNumbers1.push(i);
      }
      return pageNumbers1;
    }
  
    /*goToPage(pageNumber1: number | string) {
      if (pageNumber1 >= 1 && pageNumber1 <= this.totalPages1) {
        this.currentPage1 = pageNumber1;
        this.fetchData();
      }
    }*/

    isPopupOpen: boolean = false;

    goToPage(pageNumber1: number | string) {
      if (typeof pageNumber1 === 'number') {
        this.currentPage1 = pageNumber1;
        this.fetchData();
      }
    }

    openPopup(row: any) {
      this.selectedRow = row;
      this.isPopupOpen = true;
    }
  
    closePopup() {
      this.selectedRow = null;
      this.isPopupOpen = false;
    }

    resetData(){}
    Recherche(){}

}
