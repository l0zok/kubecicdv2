import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageTransfertComponent } from './manage-transfert.component';

describe('ManageTransfertComponent', () => {
  let component: ManageTransfertComponent;
  let fixture: ComponentFixture<ManageTransfertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageTransfertComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ManageTransfertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
