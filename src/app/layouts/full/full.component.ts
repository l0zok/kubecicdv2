import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItems } from 'src/app/shared/menu-items';
import { MenuParamItems } from 'src/app/shared/menuparam-items';
//import jwt_decode from "jwt-decode";

@Component({
  selector: 'app-full',
  templateUrl: './full.component.html',
  styleUrls: ['./full.component.scss']
})
export class FullComponent implements OnInit {

  token:any = localStorage.getItem('token');
  tokenPayload:any;

  role: any;
  constructor(private router: Router,
    public menuItems:MenuItems, public menuParamItems:MenuParamItems) {
      //this.tokenPayload = jwt_decode(this.token);
  }
  ngOnInit(): void {
    
  }

  logout(){
    //localStorage.clear();
    //localStorage.removeItem('token');
      this.router.navigate(['/']);
  }

  changePassword(){
    
  }

}
