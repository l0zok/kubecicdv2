import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { SnackbarService } from '../services/snackbar.service';
import { UserService } from '../services/user.service';
import { GlobalConstants } from '../shared/global-constants';
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit{
  
  loginForm:any = FormGroup;
  responseMessage:any;
  show:Boolean = false;
  date:any = new Date();
  annee:any = this.date.getFullYear();


  constructor(private formBuilder:FormBuilder,
    private router:Router,
    private userService:UserService,
    //public dialogRef:MatDialogRef<LoginComponent>,
    private ngxService:NgxUiLoaderService,
    private snackbarService:SnackbarService,
    private toastr:ToastrService,){}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username:[null,[Validators.required,Validators.required]],
      password: [null,Validators.required]
    })
  }

  handleSubmit(){
    this.ngxService.start();
    
    var formData = this.loginForm.value;
    var data = {
      username: formData.username,
      password: formData.password
    }
    
    this.userService.login(data).subscribe({
      next: (response: any) => {
        this.show = false;
        //alert(response.token);
        this.ngxService.stop();
        //this.dialogRef.close();
        //console.log(response);
        //localStorage.setItem('token',response.token);
        localStorage.setItem('userconnectinfo',response.data.familyName);
        this.router.navigate(['/arkham/dashboard']);
      },
      error: (error: any) => {
        this.ngxService.stop();
        this.show = true;
        if(error.error?.message) {
          this.responseMessage = error.error?.message;
        } else {
          this.responseMessage = GlobalConstants.genericError;
        }
        //this.snackbarService.openSnackBar(this.responseMessage, GlobalConstants.error);
      }
    });
  }

  pagepaiement(){
    this.router.navigateByUrl("paiement");
  }

}
