import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  url = `${environment.apiURL}`;

  constructor(private httpClient: HttpClient) { }

  getDetails(){
    

    return this.httpClient.get(this.url+"/arkham-backend/v1/payment",{
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    })
  }

  getInfoStatus(startDate: any, endDate: any): Observable<any> {
    return this.httpClient.get(this.url+"/arkham-backend/v1/payment/status?startDate="+startDate+"&"+"endDate="+endDate,{
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    })
  }

  getInfo(startDate: any, endDate: any): Observable<any> {
    return this.httpClient.get(this.url+"/arkham-backend/v1/payment/get-infos?startDate="+startDate+"&"+"endDate="+endDate,{
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    })
  }

}
