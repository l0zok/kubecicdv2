import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MarchantService {

  url = `${environment.apiURL}`;
  constructor(private httpClient:HttpClient) { }

  getMarchants(page: number, limit: number): Observable<any> {
    return this.httpClient.get(this.url+"/arkham-backend/v1/merchant?index="+page+"&"+"size="+limit,{
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    })
  }

  newMarchant(data:any,id:any){
    return this.httpClient.post(this.url+"/arkham-backend/v1/merchant?user_id="+id,data,{
      headers: new HttpHeaders().set('Content-Type',"application/json")
    })
  }

  updateMarchant(data:any,user_id:any,marchant_id:any){
    return this.httpClient.put(this.url+"/arkham-backend/v1/merchant?user_id="+user_id+"&"+"merchant.id="+marchant_id,data,{
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    })
  }

  enableMarchant(user_id:any,marchant_id:any){
    return this.httpClient.put(this.url+"/arkham-backend/v1/merchant/enable?user_id="+user_id+"&"+"merchant.id="+marchant_id,{
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    })
  }

  disableMarchant(user_id:any,marchant_id:any){
    return this.httpClient.put(this.url+"/arkham-backend/v1/merchant/disable?user_id="+user_id+"&"+"merchant.id="+marchant_id,{
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    })
  }
  
}
