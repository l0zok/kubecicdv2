import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  url = `${environment.apiURL}`;
  constructor(private httpClient:HttpClient) { }

  newRole(data:any,id:any){
    return this.httpClient.post(this.url+"/arkham-backend/v1/role?user_id="+id,data,{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }

  updateRole(data:any,user_id:any,role_id:any){
    return this.httpClient.put(this.url+"/arkham-backend/v1/role?user_id="+user_id+"&"+"role.id="+role_id,data,{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }

  getDetailRole(id:any){
    return this.httpClient.get(this.url+"/arkham-backend/v1/get/paiement/getById/"+id,{
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    })
  }

  getRoles(page: number, limit: number): Observable<any> {
    return this.httpClient.get(this.url+"/arkham-backend/v1/role?index="+page+"&"+"size="+limit,{
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    })
  }

  enableRole(user_id:any,role_id:any){
    return this.httpClient.put(this.url+"/arkham-backend/v1/role/enable?user_id="+user_id+"&"+"role.id="+role_id,{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }

  disableRole(user_id:any,role_id:any){
    return this.httpClient.put(this.url+"/arkham-backend/v1/role/disable?user_id="+user_id+"&"+"role.id="+role_id,{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }

}
