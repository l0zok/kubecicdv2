import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TransfertService {

  url = `${environment.apiURL}`;
  constructor(private httpClient:HttpClient) { }

  getTransferts(){
    return this.httpClient.get(this.url+"/arkham-transfer-service/v1/admin/transfers/",{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }

  getDetailTransfert(id:any){
    return this.httpClient.get(this.url+"/arkham-transfer-service/v1/admin/transfers/"+id,{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }

  getApiUrl(page: number, limit: number): Observable<any> {
    return this.httpClient.get(this.url+"/arkham-transfer-service/v1/admin/transfers?page="+page+"&"+"size="+limit,{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }

}
