import { TestBed } from '@angular/core/testing';

import { MarchantService } from './marchant.service';

describe('MarchantService', () => {
  let service: MarchantService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MarchantService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
