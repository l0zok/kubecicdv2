import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PaiementService {

  url = `${environment.apiURL}`;
  constructor(private httpClient:HttpClient) { }

  getPaiements(){
    return this.httpClient.get(this.url+"/arkham-backend/v1/payment/",{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }

  getDetailPaiement(id:any){
    return this.httpClient.get("/arkham-backend/v1/payment/get/paiement/getById/"+id,{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }

  getApiUrl(page: number, limit: number): Observable<any> {
    return this.httpClient.get(this.url+"/arkham-backend/v1/payment/?index="+page+"&"+"size="+limit,{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }

  SearchgetApiUrl(page: number, limit: number,startDate: any, endDate: any): Observable<any> {
    return this.httpClient.get(this.url+"/arkham-backend/v1/payment/?index="+page+"&"+"size="+limit+"&"+"startDate="+startDate+"&"+"endDate="+endDate,{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }

}
