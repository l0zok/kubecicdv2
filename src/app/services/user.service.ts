import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  //url = "http://localhost:3000";
  url = `${environment.apiURL}`;
  constructor(private httpClient:HttpClient) { }

  signup(data:any){
    return this.httpClient.post(this.url+"/arkham-backend/v1/authenticate/",data,{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }


  forgotPassword(data:any){
    return this.httpClient.post(this.url+"/arkham-backend/v1/user/forgotPassword",data,{
      headers: new HttpHeaders().set('Content-Type',"application/json")
    })
  }

  login(data:any){
    return this.httpClient.post(this.url+"/arkham-backend/v1/authenticate",data,{
      headers: new HttpHeaders().set('Content-Type',"application/json")
    })
  }

  checkToken(){
    return this.httpClient.get(this.url+"/arkham-backend/v1/user/checkToken");
  }

  changePassword(data:any){
    return this.httpClient.post(this.url+"/arkham-backend/v1/user/changePassword",data,{
      headers: new HttpHeaders().set('Content-Type',"application/json")
    })
  }

  update(data:any){
    return this.httpClient.patch(this.url+"/arkham-backend/v1/user/update",data,{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }

  updateStatut(data:any){
    return this.httpClient.patch(this.url+"/arkham-backend/v1/user/updateStatut",data,{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }

  getUser(id:any){
    return this.httpClient.get(this.url+"/arkham-backend/v1/user/getById/"+id,{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }

  delete(id:any){
    return this.httpClient.delete(this.url+"/arkham-backend/v1/user/delete/"+id,{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }




  newUser(data:any,id:any){
    return this.httpClient.post(this.url+"/arkham-backend/v1/user?user_id="+id,data,{
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    })
  }

  updateUser(data:any,user_id:any,usermodif_id:any){
    return this.httpClient.put(this.url+"/arkham-backend/v1/user?user_id="+user_id+"&"+"user.id="+usermodif_id,data,{
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    })
  }

  getDetailUser(id:any){
    return this.httpClient.get("/arkham-backend/v1/payment/get/paiement/getById/"+id,{
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    })
  }

  getUsers(page: number, limit: number): Observable<any> {
    return this.httpClient.get(this.url+"/arkham-backend/v1/user?index="+page+"&"+"size="+limit,{
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    })
  }

  enableUser(user_id:any,usermodif_id:any){
    return this.httpClient.put(this.url+"/arkham-backend/v1/user/enable?user_id="+user_id+"&"+"user.id="+usermodif_id,{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }

  disableUser(user_id:any,usermodif_id:any){
    return this.httpClient.put(this.url+"/arkham-backend/v1/user/disable?user_id="+user_id+"&"+"user.id="+usermodif_id,{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }

  getRoles(): Observable<any> {
    return this.httpClient.get(this.url+"/arkham-backend/v1/role",{
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    })
  }

  

}
