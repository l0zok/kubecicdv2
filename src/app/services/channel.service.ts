import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ChannelService {

  url = `${environment.apiURL}`;
  constructor(private httpClient:HttpClient) { }

  newChannel(data:any,id:any){
    return this.httpClient.post(this.url+"/arkham-backend/v1/channel?user_id="+id,data,{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }

  updateChannel(data:any,user_id:any,channel_id:any){
    return this.httpClient.put(this.url+"/arkham-backend/v1/channel?user_id="+user_id+"&"+"channel.id="+channel_id,data,{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }

  getDetailChannel(id:any){
    return this.httpClient.get(this.url+"/arkham-backend/v1/payment/get/paiement/getById/"+id,{
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    })
  }

  getChannels(page: number, limit: number): Observable<any> {
    return this.httpClient.get(this.url+"/arkham-backend/v1/channel?index="+page+"&"+"size="+limit,{
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    })
  }

  enableChannel(user_id:any,channel_id:any){
    return this.httpClient.put(this.url+"/arkham-backend/v1/channel/enable?user_id="+user_id+"&"+"channel.id="+channel_id,{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }

  disableChannel(user_id:any,channel_id:any){
    return this.httpClient.put(this.url+"/arkham-backend/v1/channel/disable?user_id="+user_id+"&"+"channel.id="+channel_id,{
      headers: new HttpHeaders().set('ContentType',"application/json")
    })
  }
  
}
