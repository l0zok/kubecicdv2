import { TestBed } from '@angular/core/testing';

import { HightchartsService } from './hightcharts.service';

describe('HightchartsService', () => {
  let service: HightchartsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HightchartsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
