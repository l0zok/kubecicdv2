import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RouteGuardService } from './services/route-guard.service';
import { FullComponent } from './layouts/full/full.component';
import { NewUserComponent } from './material-component/new-user/new-user.component';
import { EditUserComponent } from './material-component/edit-user/edit-user.component';
import { NewMarchantComponent } from './material-component/new-marchant/new-marchant.component';
import { TestpaieComponent } from './testpaie/testpaie.component';

const routes: Routes = [
  {path : 'login', component:LoginComponent},
  {path : 'paiement', component:TestpaieComponent},
  {path : '', component:LoginComponent},
  {
    path: 'arkham',
    component: FullComponent,
    children: [
      {
        path: '',
        redirectTo: '/arkham/dashboard',
        pathMatch: 'full',
        /*data:{
          expectedRole:['admin','user']
        }*/
      },
      {
        path: '',
        loadChildren:
          () => import('./material-component/material.module').then(m => m.MaterialComponentsModule),
          /*canActivate:[RouteGuardService],
          data:{
            expectedRole:['admin','user']
          }*/
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
        /*data:{
          expectedRole:['admin','user']
        }*/
      },
      {
        path: 'new-user', // child route path
        component: NewUserComponent, // child route component that the router renders
        /*data:{
          expectedRole:['admin']
        }*/
      },
      {
        path: 'new-marchant', // child route path
        component: NewMarchantComponent, // child route component that the router renders
        /*data:{
          expectedRole:['admin']
        }*/
      }
    ]
  },
  { path: '**', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
