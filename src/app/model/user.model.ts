export class user{
    id:number = 0;
    firstName:string='';
    lastName:string='';
    email:string='';
    contact:string='';
    username:string='';
    datasRole:string='';
}