#!/bin/sh

unset http_proxy
unset https_proxy

cd /app && ng serve --host 0.0.0.0 --port 3001 --disable-host-check
